import { createStore } from 'vuex';
import { OrderModule, ProductModule } from './modules';

const store = createStore({
  modules: {
    OrderModule,
    ProductModule,
  },
});

export {
  store
};
