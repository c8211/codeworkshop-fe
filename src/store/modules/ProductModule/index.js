import { getProductDetails } from '@/services/ProductService/ProductService';

const initialState = {
  products: [],
};

const getters = {
  products: ({ products }) => products,
};

const actions = {
  async updateProductDetails({ commit }, { productId }) {
    const productDetails = await getProductDetails({ productId });
    commit('setProductData', { ...productDetails, productId });
  },
};

const mutations = {
  setProductData(state, productData) {
    state.products[productData.productId] = productData;
  },
};

const ProductModule = {
  namespaced: true,
  state: initialState,
  actions,
  mutations,
  getters,
};
export { ProductModule };
