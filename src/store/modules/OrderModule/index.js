import { getOrder, getOrderItems } from '@/services/OrderService/OrderService';

const orderId = '1';

const initialState = {
  order: {
    orderItems: {
      items: [],
    },
  },
};

const getters = {
  order: ({ order }) => order,
};

const actions = {
  async updateOrder({ commit, dispatch }) {
    const orderHead = await getOrder({ orderId });
    const orderItems = await getOrderItems({ orderId });
    const mappedOrderItems = Object.assign(orderItems,
      { items: orderItems.items.map((item) => ({ ...item, productId: item.product })) });
    const fullOrder = { ...orderHead, orderItems: mappedOrderItems };
    await commit('setOrder', fullOrder);
    fullOrder.orderItems.items.forEach((item) => dispatch('ProductModule/updateProductDetails', { productId: item.product }, { root: true }));
  },
};

const mutations = {
  setOrder(state, order) {
    state.order = order;
  },
};

const OrderModule = {
  namespaced: true,
  state: initialState,
  mutations,
  getters,
  actions,
};
export { OrderModule };
