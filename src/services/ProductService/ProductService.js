import axios from 'axios';

const baseUrl = 'https://api.predic8.de/shop/v2/products'; 

const getProductDetails = async ({ productId }) => {
  const url = `${baseUrl}/${productId}`;
  const { data: productDetails } = await axios.get(url).then((result) => result);
  return productDetails;
};

export {
  getProductDetails,
};
