import { createApp } from 'vue'
import { store } from '@/store';
import './style.css'
import App from './App.vue'

const app = createApp(App);
app.config.globalProperties.$filters = {
  asEuro(value) {
    return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(value);
  }
}
app.use(store);
app.mount('#app');
