// /////////////////////////////////////////////////////////////////////////////////
// Refer to the entire list of global config settings here:
// https://github.com/nightwatchjs/nightwatch/blob/master/lib/settings/defaults.js#L16
//
// More info on test globals:
//   https://nightwatchjs.org/gettingstarted/configuration/#test-globals
//
// /////////////////////////////////////////////////////////////////////////////////

module.exports = {
  src_folders: ['tests/e2e/specs'],
  page_objects_path: ['tests/e2e/page-objects'],
  custom_assertions_path: ['tests/e2e/custom-assertions'],
  custom_commands_path : ['tests/e2e/custom-commands'],
  output_folder: 'tests/e2e/reports',
  test_settings: {
    default: {
      launch_url: 'http://localhost:8080',
      webdriver: {
        start_process: true,
        server_path: ''
      }
    },
    'chromeDesktop': {
      desiredCapabilities: {
        browserName: 'chrome'
      }
    }
  },

  // this controls whether to abort the test execution when an assertion failed and skip the rest
  // it's being used in waitFor commands and expect assertions
  abortOnAssertionFailure: false,

  // this will overwrite the default polling interval (currently 500ms) for waitFor commands
  // and expect assertions that use retry
  waitForConditionPollInterval: 500,

  // default timeout value in milliseconds for waitFor commands and implicit waitFor value for
  // expect assertions
  waitForConditionTimeout: 5000,

  default: {
    /*
    The globals defined here are available everywhere in any test env
    */
    /*
    myGlobal: function() {
      return 'I\'m a method';
    }
    */
  },
};
