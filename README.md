# **fe-codeworkshop**

This project is used for conducting coding-interviews.\
The following tasks have to be prepared **2h before joining the live interview**.

## **Tools needed for the interview:**
* IDE (Visual Studio Code, IntelliJ, Eclipse etc)
* Git
* Node/NPM
* Chrome browser

## **Project setup**

Install dependencies:
```bash
npm install
```

Build and start server for development at http://localhost:8080
```bash
npm run dev
```

Run end-to-end-tests:
```bash
npm run test:e2e
```
(Please note: Your installed chromedriver version (see [package.json](./package.json)) needs to match with your installed chrome version.)

## **Shop API** (used within project)
* https://api.predic8.de/api-docs/ui/fruit-shop-api-v2-2-0

## **Interview tasks**

### Project exploration: (~ 30 min)

1. Investigate the page content (DOM)
2. Explore the executed API calls within your browser's network-tab
3. Explore the project structure and file contents

### Prepare for the following interview questions: (~ 30 min)

1. What content does the app provide?
2. What are your main steps when exploring such a new/unknown project? (just describe the main steps)
3. How would you explain the key "architectural" aspects of this project to another developer?
4. How would you rate the project? What do you like/dislike? What would you do differently?

### Writing code: (~ 60 min)

1. Add a product picture to each item. (The [Shop API](https://api.predic8.de/api-docs/ui/fruit-shop-api-v2-2-0) offers you corresponding product-image urls)
2. Something is wrong with the displayed prices (showing "NaN €" on every item) - Can you fix the issue?
3. The e2e-tests are failing. Can you fix the issue?
